@include('includes.header')
    <div class="row cms-row" style="padding-left:20px;	padding-right:20px;">


        <div class="col col-sm-9"  style=" padding-top:20px;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>


                <div class="carousel-inner">


                    <div class="item active">
                        <img src="{{url('public/assets/images/mayor.jpg')}}" alt="Mayor" class="img-responsive sliderImage">
                        <div class="carousel-caption">
                            <h3>Mayor</h3>
                        </div>
                    </div>

                    <div class="item">
                        <img src="{{url('public/assets/images/deputy_mayor.jpg')}}" alt="Deputy Mayor" class="img-responsive sliderImage">
                        <div class="carousel-caption">
                            <h3>Deputy Mayor</h3>
                        </div>
                    </div>

                    <div class="item">
                        <img src="{{url('public/assets/images/gaupalika_office.jpg')}}" alt="Gaupalika Office" class="img-responsive sliderImage">
                        <div class="carousel-caption">
                            <h3>Gaupalika Office</h3>
                        </div>
                    </div>
                </div>


                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>



        <div class="col col-sm-3" style=" padding-top:20px;">
            <div class="form-group has-feedback has-feedback-left">
                <input id="stitle" class="form-control main-search" placeholder="search" />
                <a onclick="searchHome();" id="sbutton" style="position:absolute;right:0px;padding:10px;" class="  glyphicon glyphicon-search search-icon-button"></a>
            </div>
            <style>
                #sbutton:hover {

                }
            </style>
            <script>
                function searchHome()
                {
                    window.location.href = "/NationalPortal/search?title=" + $("#stitle").val();
                }
            </script>




            <div class="list-group">
				<span class="list-group-item active">
					महत्त्वपूर्ण सरकारी निर्णयहरु
				</span>
                <a href="http://www.opmcm.gov.np/download-documents/?wpdmc=cabinet-decision" class="list-group-item">मन्त्रिपरिषद्का निर्णयहरु</a>
                <a href="http://www.opmcm.gov.np/download-documents/?wpdmc=samiti-decision" class="list-group-item">मन्त्रिपरिषद् समितिका निर्णयहरु</a>
                <a href="http://www.opmcm.gov.np/download-documents/?wpdmc=secretary-decision" class="list-group-item">सचिव बैठकका निर्णयहरु</a>
                <a href="http://www.npc.gov.np/en/category/weekly_decisions" class="list-group-item">राष्ट्रिय योजना आयोगको निर्णयहरु</a>
                <a href="http://www.moha.gov.np/uploads/newsFiles/7940_bf58f4012a064a082e166d0dbfcf1706_1491155898_2.pdf" class="list-group-item">सार्वजनिक विदाहरु</a>

            </div>




        </div>
    </div>


    <div class="row cms-row" >
        <div class="col col-sm-6" style="padding-right:0px;">


            <div class="col col-md-6" style="padding-top:20px;">
                <div class="outer-div-big outer-red-top">
                    <h3 class="home-group-menu outer-red-color">
                        सरकार
                        <a href="view-page7d36.html?id=51" class="more-link"><i class="glyphicon glyphicon-menu-right more-home red-back"></i></a>
                    </h3>
                    <img src="{{url('public/assets/images/government-pic.jpg')}}" style="width:100%;" />
                    <div class="list-group">
                        <a href="view-page7d36.html?id=51" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/legislative.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">व्यवस्थापिका</h4><span class="sub-title">२०१७-०८-१०</span></div>
                        </a>
                        <a href="view-page9e43.html?id=25" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/executive.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">कार्यपालिका</h4><span class="sub-title">२०१७-०८-१०</span></div>
                        </a>
                        <a href="view-pagebb33.html?id=47" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/judiciary.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">न्यायपालिका</h4><span class="sub-title">२०१७-०७-१४</span></div>
                        </a>

                    </div>
                </div>
            </div>



            <div class="col col-md-6 " style="padding-top:20px;">
                <div class="outer-div-big outer-blue-top">
                    <h3 class="home-group-menu outer-blue-color">
                        विद्युतीय सेवा
                        <a href="#" data-toggle="modal" data-target="#myEserviceForm" class="more-link"><i class="glyphicon glyphicon-menu-right more-home blue-back"></i></a>
                    </h3>
                    <img src="{{url('public/assets/images/eservice-p.jpg')}}" style="width:100%;" />

                    <div class="overflow-div">
                        <div class="list-group" style="position:relative">
                            <div style="position:absolute;">
                                <a href="NationalPortal/nrbService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/ppmo.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">Foreign Exchange Rate</h4><span class="sub-title">NRB</span></div>
                                </a>
                            </div>

                            <div>
                                <a href="ocrService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/running_business.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            कम्पनी खोजी
                                        </h4><span class="sub-title">OCR</span></div>
                                </a>
                            </div>

                            <div>
                                <a href="nrbService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/nrb_small.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            विदेशी विनिमय दर
                                        </h4><span class="sub-title">NRB</span></div>
                                </a>
                            </div>
                            <div>
                                <a href="lrimsAppService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/dolrm_small.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            आवेदन स्थिति विवरण
                                        </h4><span class="sub-title">DOLRM</span></div>
                                </a>
                            </div>




                        </div>
                    </div>
                </div>
            </div>




            <div class="modal fade" id="myEserviceForm" role="dialog" style="display: none;">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #2A7DBE !important;"> <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title" syle="color:white !important;">
                                विद्युतीय सेवा

                            </h4>
                        </div>
                        <div class="modal-body">


                            <div>
                                <a href="gunasoService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/opmcm.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            हेल्लो सरकार
                                        </h4><span class="sub-title">OPMCM</span></div>
                                </a>
                            </div>


                            <div>
                                <a href="ppmoService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/ppmo.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            सार्बजनिक खरिद अनुगमन कार्यालय
                                        </h4><span class="sub-title">PPMO</span></div>
                                </a>
                            </div>

                            <div>
                                <a href="lPDetailsService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/dolrm_small.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            भूमिसुधार सम्पत्ति विवरण
                                        </h4><span class="sub-title">DOLRM</span></div>
                                </a>
                            </div>

                            <div>
                                <a href="pdssService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/dolrm_small.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            निजामती किताबखानाको विद्युतीय सेवा
                                        </h4><span class="sub-title">DOCPR</span></div>
                                </a>
                            </div>
                            <div>
                                <a href="bptsService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/deptkmc.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            काठमाडौँ महानगरपालिकाको विद्युतीय सेवा
                                        </h4><span class="sub-title">KMC</span></div>
                                </a>
                            </div>
                            <div>
                                <a href="pStatusService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/deptpass.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            राहदानी विभागको विद्युतीय सेवा
                                        </h4><span class="sub-title">DOP</span></div>
                                </a>
                            </div>

                            <div>
                                <a href="pscService.html" class="list-group-item clearfix hg-list-border">
                                    <div class="pull-left"><img src="{{url('public/assets/images/psc.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                                    <div>
                                        <h4 class="main-title">
                                            लोक सेवा आयोगको विद्युतीय सेवा
                                        </h4><span class="sub-title">PSC</span></div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="col col-sm-6" style="padding-left:0px;">




            <div class="col col-md-6" style="padding-top:20px;">
                <div class="outer-div-big outer-yellow-top">
                    <h3 class="home-group-menu outer-yellow-color">
                        नागरिक
                        <a href="view-page2568.html?id=49" class="more-link"><i class="glyphicon glyphicon-menu-right more-home yellow-back"></i></a>
                    </h3>
                    <img src="{{url('public/assets/images/citizen-pic.jpg')}}" style="width:100%;" />
                    <div class="list-group">
                        <a href="view-page2568.html?id=49" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/gunasoimg.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">गुनासो</h4><span class="sub-title">२०१७-०८-११</span></div>
                        </a>
                        <a href="view-page98dc.html?id=45" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/bloodbank.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">ब्लड बैंक</h4><span class="sub-title">२०१७-०८-०१</span></div>
                        </a>
                        <a href="view-pagedf0b.html?id=162" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/oldage.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">वृद्धाश्रम</h4><span class="sub-title">२०१७-०८-११</span></div>
                        </a>

                    </div>
                </div>
            </div>


            <div class="col col-md-6" style="padding-top:20px;">
                <div class="outer-div-big outer-green-top">
                    <h3 class="home-group-menu outer-green-color">
                        व्यवसाय सञ्चालन
                        <a href="view-pagebc58.html?id=78" class="more-link"><i class="glyphicon glyphicon-menu-right more-home green-back"></i></a>
                    </h3>
                    <img src="{{url('public/assets/images/runningbusiness.jpg')}}" style="width:100%;" />
                    <div class="list-group">
                        <a href="view-pagebc58.html?id=78" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/running_business.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">व्यवसाय सञ्चालन</h4><span class="sub-title">२०१७-०८-२२</span></div>
                        </a>
                        <a href="view-pagee106.html?id=144" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/economic_senario.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">आर्थिक परिदृश्य</h4><span class="sub-title">२०१७-०८-१०</span></div>
                        </a>
                        <a href="view-page9dbc.html?id=84" class="list-group-item clearfix hg-list-border">
                            <div class="pull-left"><img src="{{url('public/assets/images/invest_in_np.jpg')}}" style="height:50px;width:50px" class="" alt=""></div>
                            <div>
                                <h4 class="main-title">लगानी</h4><span class="sub-title">२०१७-०८-२२</span></div>
                        </a>

                    </div>
                </div>
            </div>





        </div>
    </div>







    </div>


    </div>











    <div class="row cms-row" style="width:100%; padding-left:20px;	padding-right:0px;">
        <div class="col col-md-9" style="padding-left:15px;	padding-right:10px; margin-top:20px;">

            <div class="tab-container" style="min-height:300px;">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#eGovernance">विद्युतीय सुशासनतर्फको अग्रसरता</a></li>
                    <li><a data-toggle="tab" href="#EForms">ई-फारम</a></li>
                    <li><a data-toggle="tab" href="#EmergenyServices">आपतकालीन सेवा</a></li>
                    <li><a data-toggle="tab" href="#OtherUtilites">अन्य उपयोगिता</a></li>
                    <li><a data-toggle="tab" href="#newsFeeds">समाचार</a></li>
                </ul>



                <div class="tab-content">

                    <div id="eGovernance" class="tab-pane fade in active menu-item"  style="min-height:335px;" >

                    </div>

                    <div id="OtherUtilites" class="tab-pane fade" style="min-height:335px;">

                        <div class="row">

                            <div class="col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active">
                                        <a href="#Horoscope" data-toggle="tab">
                                            Horoscope
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#Weather" data-toggle="tab">
                                            Weather
                                        </a>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-xs-9">
                                <div class="tab-content">



                                    <div class="tab-pane active" id="Horoscope">
                                        <div>
                                            <div class="col col-md-12 horoscope-outer-container">
                                                <a onclick="getHoroscope('aries')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Aries%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('aries')" data-toggle="modal" data-target="#myHoroscopeModal">Aries
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('taurus')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Taurus%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('taurus')" data-toggle="modal" data-target="#myHoroscopeModal">Taurus
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('gemini')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Gemini%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('gemini')" data-toggle="modal" data-target="#myHoroscopeModal">Gemini
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('cancer')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Cancer%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('cancer')" data-toggle="modal" data-target="#myHoroscopeModal">Cancer
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('leo')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Leo%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('leo')" data-toggle="modal" data-target="#myHoroscopeModal">Leo
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('virgo')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Virgo%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('virgo')" data-toggle="modal" data-target="#myHoroscopeModal">Virgo
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('libra')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Libra%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('libra')" data-toggle="modal" data-target="#myHoroscopeModal">Libra
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('scorpio')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20scorpio%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('scorpio')" data-toggle="modal" data-target="#myHoroscopeModal">Scorpio
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('sagittarius')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Sagittarius%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('sagittarius')" data-toggle="modal" data-target="#myHoroscopeModal">Sagittarius
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('capricorn')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Capricorn%20-WF.png')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('capricorn')" data-toggle="modal" data-target="#myHoroscopeModal">Capricorn
                                                        </button>
                                                    </div>
                                                </a>

                                                <a onclick="getHoroscope('aquarius')">
                                                    <div class="col col-xs-3 horoscope-container">
                                                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Aquarius%20-WF.jpg')}}" class="horoscopeImage">
                                                        <button class="btn-link" onclick="getHoroscope('aquarius')" data-toggle="modal" data-target="#myHoroscopeModal">Aquarius
                                                        </button>
                                                    </div>
                                                </a>

                                                <span onclick="getHoroscope('pisces')">
                    <div class="col col-xs-3 horoscope-container" style="padding-top:10px">
                        <img src="{{url('public/assets/images/Design-Icon/blue/Astro%20Pisces%20-WF.jpg')}}" class="horoscopeImage">
                            <button class="btn-link"
                                    onclick="getHoroscope('pisces')"
                                    data-toggle="modal" data-target="#myHoroscopeModal">Pisces
                            </button>
                    </div>
                    </span>

                                            </div>
                                        </div>
                                    </div>












                                    <div class="tab-pane" id="Weather">
                                        <div class="bhoechie-tab-content  t-content">

                                            <button class="btn-link" onclick="getWeather('kathmandu')" data-toggle="modal" data-target="#myHoroscopeModal">Kathmandu
                                            </button>

                                            <button class="btn-link" onclick="getWeather('BHAIRAWA')" data-toggle="modal" data-target="#myHoroscopeModal">Bhairawa
                                            </button>

                                            <button class="btn-link" onclick="getWeather('BIRATNAGAR')" data-toggle="modal" data-target="#myHoroscopeModal">Biratnagar
                                            </button>

                                            <button class="btn-link" onclick="getWeather('JUMLA')" data-toggle="modal" data-target="#myHoroscopeModal">Jumla
                                            </button>

                                            <button class="btn-link" onclick="getWeather('POKHARA')" data-toggle="modal" data-target="#myHoroscopeModal">Pokhara
                                            </button>

                                            <button class="btn-link" onclick="getWeather('SIMRA')" data-toggle="modal" data-target="#myHoroscopeModal">Simra
                                            </button>

                                            <button class="btn-link" onclick="getWeather('SURKHET')" data-toggle="modal" data-target="#myHoroscopeModal">Surkhet
                                            </button>
                                        </div>


                                    </div>
                                </div>

                            </div>







                        </div>
                    </div>


                    <div id="EmergenyServices" class="tab-pane fade">
                        <div class="row"  style="min-height:335px;">
                            <div class="col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#HelpLineNumber" data-toggle="tab">Helpline Numbers</a></li>
                                    <li><a href="#Ambulances" data-toggle="tab">Ambulances</a></li>
                                    <li><a href="#FireBrigade" data-toggle="tab">Fire Brigade</a></li>
                                    <li><a href="#HelloSarkar" data-toggle="tab">Hello Sarkar</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-9">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="HelpLineNumber">
                                        <div>
                                            <br/>
                                            <span>Helpline numbers including mobile police Vans<br/></span>

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr style="background:#e0e8f3;">
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Telephone Numbers
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>

                                                <tr>
                                                    <td>
                                                        Police Control
                                                    </td>
                                                    <td>
                                                        100
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Police Emergency Number
                                                    </td>
                                                    <td>
                                                        4228435/4226853
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Metropolitan Police Range (Kathmandu)
                                                    </td>
                                                    <td>
                                                        4261945/4261790
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Metropolitan Police Range (Lalitpur)
                                                    </td>
                                                    <td>
                                                        5521207
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Metropolitan Police Range (Bhaktapur)
                                                    </td>
                                                    <td>
                                                        6614821
                                                    </td>
                                                </tr>


                                                </tbody>
                                            </table>
                                        </div>

                                    </div>






                                    <div class="tab-pane" id="Ambulances">


                                        <div>

                                            <br/>
                                            <span>Helpline numbers including mobile police Vans</span>
                                            <br />
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr style="background:#e0e8f3;">
                                                    <th>

                                                        Ambulances
                                                    </th>
                                                    <th>
                                                        Telephone Numbers
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>

                                                <tr>
                                                    <td>
                                                        Ambulance Bishal Bazar, New Road
                                                    </td>
                                                    <td>
                                                        4244121
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Ambulance Kathmandu Model Hospital, Bagbazar
                                                    </td>
                                                    <td>
                                                        4240805,4240806
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Ambulance Lalitpur Municipality, Pulchowk
                                                    </td>
                                                    <td>
                                                        5527003
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Ambulance Nepal Chamber of Commerce
                                                    </td>
                                                    <td>
                                                        4230213
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Ambulance Nepal Red Cross Society
                                                    </td>
                                                    <td>
                                                        4228094
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Ambulance Paropakar
                                                    </td>
                                                    <td>
                                                        4251614, 4211959
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>
                                                        Ambulance Shiva Sakti Yuba Sewa, Kathmandu
                                                    </td>
                                                    <td>
                                                        4478111
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Lions Club of Kathmandu Central, Kathmandu
                                                    </td>
                                                    <td>
                                                        4472211
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Strongtsen Bhrikuti Samajik Tatha Sanskritik Uthan Sangh, Boudha
                                                    </td>
                                                    <td>
                                                        4473166
                                                    </td>
                                                </tr>


                                                </tbody>
                                            </table>




                                        </div>


                                    </div>





                                    <div class="tab-pane" id="FireBrigade">

                                        <div>
                                            <br/>
                                            <p>Please contact at hotline number <b>101</b> for Fire Brigade Service.</p>
                                        </div>

                                    </div>





                                    <div class="tab-pane" id="HelloSarkar">
                                        <div>
                                            <br/>
                                            <p style=""	>Why hello Sarkar?</p>
                                            <ol>
                                                <li>
                                                    To give information about event / accident.
                                                </li>
                                                <li>
                                                    To seek rescue help in event / accident.
                                                </li>
                                                <li>
                                                    To give information on women violence.
                                                </li>
                                                <li>
                                                    To complain or suggest with regard to public service.
                                                </li>
                                                <li>
                                                    To complain in irregularities while delivering in government service.
                                                </li>
                                            </ol>
                                            <br/>

                                            <p>How to contact Hello Sarkar ?</p>
                                            <ul>
                                                <li>
                                                    Call Phone number: 1111
                                                </li>
                                                <li>
                                                    Send SMS to 1111
                                                </li>
                                                <li>
                                                    Email to 1111@nepal.gov.np
                                                </li>
                                                <li>
                                                    Send fax to 1100
                                                </li>
                                                <li>
                                                    Complain by sending letter or in person.
                                                </li>
                                            </ul>

                                            <p>For more Information :</p>
                                            <br>
                                            <p>Website : <a href="http://www.opmcm.gov.np/">www.opmcm.gov.np</a></p>
                                            <p>[Office of the Prime Minister and Council of Ministers, Singhdurbar, Kathmandu, Nepal]</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div id="newsFeeds" class="tab-pane fade" style="max-height:335px; overflow-y:scroll; overflow-x:hidden">

                    </div>

                    <div id="EForms" class="tab-pane fade">




                    </div>

                    <div id="OtherUtilites" class="tab-pane fade">
                        <p>Some content in menu 2.</p>
                    </div>

                    <div id="menu2" class="tab-pane fade">
                        <p>Some content in menu 2.</p>
                    </div>

                    <div id="menu2" class="tab-pane fade">
                        <p>Some content in menu 2.</p>
                    </div>


                </div>
            </div>
        </div>

        <div class="col col-md-3" style=" margin-top:25px; padding-left:20px; padding-right:5px;">
            <div class="list-group">
				<span class="list-group-item active">
					हेलो सरकार  twitter मा

					<i class="fa fa-twitter" aria-hidden="true"></i>
				</span>
                <a style="margin-top:-5px; border-top:0px;" class="twitter-timeline" data-width="305" data-height="353" href="https://twitter.com/hello_sarkar?lang=en">Tweets from Hello Sarkar</a>

                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </div>

    </div>


    <script>
        $(document).ready(function(){


            $.ajax({
                url: '/NationalPortal/eGovInitiatives',
                type: 'GET',
                data: {lang:'NP'},
                beforeSend: function () {}
            }).done(function (resultHtml) {
                $('#eGovernance').html(resultHtml);

            });

            $.ajax({
                url: '/NationalPortal/admin/eDownloadForms-list-home',
                type: 'GET',
                beforeSend: function () {}
            }).done(function (resultHtml) {
                $('#EForms').html(resultHtml);

            });

            $.ajax({
                url: '/NationalPortal/newsFeed',
                type: 'GET',
                beforeSend: function () {}
            }).done(function (resultHtml) {
                $('#newsFeeds').html(resultHtml);

            });



            //Script for verticle Tab

            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });



        });

    </script>
    <div class="modal fade" role="dialog" id="myHoroscopeModal">
        <div class="modal-dialog">


            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <div id="utilitiesModal">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



    <script>

        var AppendDiv=$('#utilitiesModal');
        var title=$('#appendiv-title');

        function getHoroscope(horoscopeValue){
            AppendDiv.empty();
            $.ajax({
                url:'/NationalPortal/horoscope',
                data:{'HoroscopeValue':horoscopeValue},
                success:function(data){
                    AppendDiv.html(data);
                }
            });
        }


        function getWeather(weatherValue){
            AppendDiv.empty();
            title.empty();
            title.html('<h2>Weather</h2>');
            $.ajax({
                url:'/NationalPortal/weather',
                data:{'UrlValue':weatherValue},
                success:function(data){
                    AppendDiv.html(data);
                }
            });
        }
    </script>
    @include('includes.footer')