<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{SITE_NAME}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('public/assets/images/favico.png')}}">
    <link rel="stylesheet" href="{{url('public/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('public/assets/css/style.css')}}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('public/assets/css/dropdown.css')}}">
    <script src="{{url('public/assets/js/jquery-3.2.1.js')}}"></script>
    <script src="{{url('public/assets/js/bootstrap.js')}}"></script>
    <script src="{{url('public/assets/js/app.js')}}"></script>
    <link rel="stylesheet" href="{{url('public/assets/css/verticle-tab.css')}}'">
    <div id="top-header">
        <div style="float:right"><span class="nepali-english-span">        <a class="SignIn-class" href="#">Nepali</a>        |    </span>        <span>        <a class="SignIn-class" href="#">English</a>    </span> </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <div class="navbar-header"> <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="/NationalPortal/view-page?id=bs-example-navbar-collapse-1" aria-expanded="false">        <span class="sr-only">Toggle navigation</span>        <span class="icon-bar"></span>        <span class="icon-bar"></span>        <span class="icon-bar"></span>      </button>            <a style="margin-bottom:0px;">				</a><a class="navbar-brand" href="{{url('/')}}">					<img src="{{url('public/assets/images/nepal_logo.png')}}">				</a>
                <p class="logo-heading">{{SITE_NAME}} <span class="logo-sub-heading">Government of Nepal</span></p>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav  navbar-right">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu row">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="list-group"><a href="{{url('history')}}">History </a></li>
                                    <li class="list-group"><a href="#">Culture  </a></li>
                                    <li class="list-group"><a href="#">Geography  </a></li>
                                    <li class="list-group"><a href="#">Language </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects & Programs<b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu row">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="list-group"><a href="#">Current Plan </a></li>
                                    <li class="list-group"><a href="#">Projects </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li><a href="#">Citizen </a></li>
                    <li><a href="#">Tourism </a></li>
                    <li><a href="#">Education </a></li>
                    <li><a href="#">News & Events </a></li>
                    <li><a href="#">Contact </a></li>
                </ul>
            </div>
        </div>
    </nav>

