<footer class="site-footer" style="margin-top: 230px;">
    <div class="col col-md-12 footer-container" style="">
        <div class="col col-md-3 footer-content" style="">
            <h3 style="font-weight: normal; color: white; font-size: 15px;" class="footer-headline">उपायोगी लिङ्कहरु</h3>
            <ul style="">
                <li style=""><a href="view-page015d.html?id=182" style="font-weight: bold;">Some Link</a></li>
                <li style="font-weight: normal;"><a href="view-page0f0e.html?id=125">Some Link</a></li>
                <li style="font-weight: normal;"><a href="#" data-toggle="modal" data-target="#ForumModal">Some Link</a></li>
                <li style="font-weight: normal;"><a href="view-page7567.html?id=137">Some Link </a></li>

                <li style="font-weight: normal;"><a href="view-page39d7.html?id=145"> Some Link</a></li>
                <li style="font-weight: normal;"><a href="https://nepal.gov.np/NationalPortal/feedback-new">Some Link </a></li>
            </ul>
        </div>
        <div class="col col-md-3 footer-content" style="font-weight: normal;">
            <h3 style="color:white;font-size:15px;" class="footer-headline">Some Link</h3>
            <ul>
                <li><i class="fa fa-facebook-official" aria-hidden="true"></i><a href="https://www.facebook.com/" target="_blank">Some Link </a></li>
                <li><i class="fa fa-twitter-square" aria-hidden="true"></i><a href="https://www.twitter.com/" target="_blank">Some Link </a></li>
            </ul>
            <div class="col col-md-12">
                <h4 class="footer-headline">Some Link</h4> <a href="#"><img src="{{url('public/assets/images/google-store-icon.png')}}" alt=""></a> <a href="#"><img src="{{url('public/assets/images/apple-store-icon.png')}}" style="margin-left:15px" alt=""></a>                </div> <span style="text-align:center; width:100%;  color:white;"> </span> <span style="text-align:center; width:100%;  color:white;">(सिघ्र आउँदैछ)</span>            </div>
        <div class="col col-md-3 footer-content" style="font-weight: normal;">
            <h3 style="color:white;font-size:15px;" class="footer-headline">Some Link</h3>
            <ul>
                <li><a href="http://www.opmcm.gov.np/" target="_blank">प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय</a></li>
                <li><a href="http://www.moha.gov.np/" target="_blank">गृह मन्त्रालय </a></li>
                <li><a href="http://www.mof.gov.np/np" target="_blank">अर्थ मन्त्रालय </a></li>
                <li><a href="http://www.psc.gov.np/" target="_blank">लोक सेवा आयोग</a></li>
                <li><a href="http://nepalpassport.gov.np/np" target="_blank">राहदानी विभाग </a></li>

            </ul>
        </div>
        <div class="col col-md-3 footer-content" style="font-weight: normal;">
            <h3 style="color:white;font-size:15px;" class="footer-headline">सम्पर्क</h3>
            <ul>

                <li>राष्ट्रिय सूचना प्रविधि केन्द्र </li>


                <li>सिंहदरवार, काठमाडौं</li>
                <li>फोन: +९७७-१- ४२११९१७, ४२११७१०, ४२११५२७</li>
                <li>फ्याक्स:+९७७-१-४२४३३६२</li>
                <li>इमेल: info@nitc.gov.np</li>
            </ul>
        </div>
    </div>
</footer>

<div class="section-copyright" style="font-weight: normal;">
    <p> © २०१७ , नेपाल सरकार, राष्ट्रिय सूचना प्रविधि केन्द्र, सर्वाधिकार</p>
</div>

<div class="modal fade" id="myModal" role="dialog" style="font-weight: normal;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2A7DBE !important;"> <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Login!</h4>
            </div>
            <div class="modal-body">
                <div class="form-group"> <input type="text" class="form-control" placeholder="Username"> </div>
                <div class="form-group"> <input type="password" class="form-control" placeholder="Password"> </div>
            </div>
            <div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal">Login</button> </div>
        </div>
    </div>
</div>
