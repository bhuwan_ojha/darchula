$(function(){
	$('#passwordResetForm').validate({
		rules:{
			"userPasswordTO.userName":{
				required:true,
			},
			"userPasswordTO.email":{
				required:true,
				email:true
			}
		},
		messages:{
			"userPasswordTO.userName":{
				required:"Please enter your username."
			},
			"userPasswordTO.email":{
				required:"Please enter your email address.",
				email:"Please provide a valid email address."
			}
		}
	});
});